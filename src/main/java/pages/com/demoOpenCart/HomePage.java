package pages.com.demoOpenCart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		
		this.driver = driver;
		//This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
		
	}
	
	
	//All WebElements are identified by @FindBy annotation
	
	@FindBy(linkText = "My Account")
	WebElement MyAccountButton;
	
	@FindBy(linkText = "Register")
	WebElement RegistrationButton;
	
	@FindBy(linkText = "Login")
	WebElement LoginButton;
	
//	@FindBy(xpath = "")
//	WebElement SearchAnyProduct;
//	
//	@FindBy(xpath = "")
//	WebElement SearchButton;
	
	//All methods using these WebElements.
	
	public void ClickOnMyAccount() {
		
		MyAccountButton.click();
	}
	
	public void ClickOnLoginButton() {
		LoginButton.click();
	}
	
	public void ClickOnregistrationButton() {
		RegistrationButton.click();
	}
}





