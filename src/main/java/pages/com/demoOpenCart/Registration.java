package pages.com.demoOpenCart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import net.bytebuddy.dynamic.TypeResolutionStrategy.Passive;

public class Registration {
	
WebDriver driver;
	
	public Registration(WebDriver driver) {
		
		this.driver = driver;
		//This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
		
	}
	
	
	//All WebElements are identified by @FindBy annotation
	
	@FindBy(xpath = "//INPUT[@id='input-firstname']")
	WebElement FirstName;

	@FindBy(xpath = "//INPUT[@id='input-lastname']")
	WebElement LastName;

	@FindBy(xpath = "//INPUT[@id='input-email']")
	WebElement Email;
	
	@FindBy(xpath = "//INPUT[@id='input-telephone']")
	WebElement Telephone;
	
	@FindBy(xpath = "//INPUT[@id='input-password']")
	WebElement Password;
	
	@FindBy(xpath = "//INPUT[@id='input-confirm']")
	WebElement ConfirmPassword;
	
	@FindBy(xpath = "//INPUT[@type='checkbox']")
	WebElement privacyPolicy;
	
	@FindBy(css = "#content > form > div > div > input.btn.btn-primary")
	WebElement SumitRegistrationButton;
	
	@FindBy(xpath="//DIV[@class='alert alert-danger alert-dismissible']")
	WebElement ErrorMessagefor_empty_Privacy_Policy;
	
	@FindBy(xpath = "//DIV[@class='text-danger'][text()='First Name must be between 1 and 32 characters!']")
	WebElement ErrorMessageforRegistration_FirstName;
	
	@FindBy(xpath = "//DIV[@class='text-danger'][text()='Last Name must be between 1 and 32 characters!']")
	WebElement ErrorMessageforRegistration_LastName;
	
	@FindBy(xpath = "//DIV[@class='text-danger'][text()='E-Mail Address does not appear to be valid!']")
	WebElement ErrorMessageforRegistration_Email;
	
	@FindBy(xpath = "//DIV[@class='text-danger'][text()='Telephone must be between 3 and 32 characters!']")
	WebElement ErrorMessageforRegistration_Telephone;
	
	@FindBy(xpath = "//DIV[@class='text-danger'][text()='Password must be between 4 and 20 characters!']")
	WebElement ErrorMessageforRegistration_Password;
	
	@FindBy(xpath = "//DIV[@class='text-danger'][text()='Password confirmation does not match password!']")
	WebElement ErrorMessageforRegistration_ConfirmPass;
	
	public void FullRegistration (String firstname, String lastname, String email, String telephone, String password, String confirmpassword) {
		
		FirstName.sendKeys(firstname);
		LastName.sendKeys(lastname);
		Email.sendKeys(email);
		Telephone.sendKeys(telephone);
		Password.sendKeys(password);
		ConfirmPassword.sendKeys(confirmpassword);
		privacyPolicy.click();
		SumitRegistrationButton.click();
		
	}
	
	
	public void FullRegistration_without_privacyPolicy (String firstname, String lastname, String email, String telephone, String password, String confirmpassword) {
		
		FirstName.sendKeys(firstname);
		LastName.sendKeys(lastname);
		Email.sendKeys(email);
		Telephone.sendKeys(telephone);
		Password.sendKeys(password);
		ConfirmPassword.sendKeys(confirmpassword);
		//privacyPolicy.click();
		SumitRegistrationButton.click();
		
	}
	
	public String verify_Error_on_Empty_Privact_Policy() {
		
		//ErrorMessagefor_empty_Privacy_Policy.getText();
		return ErrorMessagefor_empty_Privacy_Policy.getText();
	}
	
	
	public String verify_Error_for_Empty_Field_FirstName() {
		
		return ErrorMessageforRegistration_FirstName.getText();
	}
	
	public String verify_Error_for_Empty_Field_LastName() {
		
		return ErrorMessageforRegistration_LastName.getText();
	}
	
	public String verify_Error_for_Empty_Field_Email() {
		
		return ErrorMessageforRegistration_Email.getText();
	}
	
	public String verify_Error_for_Empty_Field_Telephone() {
		
		return ErrorMessageforRegistration_Telephone.getText();
	}
	
	
	public String verify_Error_for_Empty_Field_Password() {
		
		return ErrorMessageforRegistration_Password.getText();
	}
	
	
	public String verify_Error_for_Empty_Field_ConfirmPass() {
		
		return ErrorMessageforRegistration_ConfirmPass.getText();
	}
	
	
	
	
	
	
	
	
}
